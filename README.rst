Developer VM
============

Pre-requisites
--------------

You will need to install:

* `virtualbox <https://www.virtualbox.org/>`_
* `vagrant <https://www.vagrantup.com/>`_
* `python3 <https://www.python.org/>`_
* `poetry <https://python-poetry.org/>`_

.. _full-documentation:

Full Documentation
------------------

To produce the full documentation for this VM you will need to setup the Python
virtual environment.

.. code-block:: bash

   poetry install
   poetry shell

Once in the virtual environment the full documentation can be generated with:

.. code-block:: bash

   cd docs
   make html

The documentation is then available at ``docs/build/html/index.html``


Using the VM
------------

To use the VM run the following.

.. code-block:: bash

   vagrant up

This sets up the VM. Once running you enter the VM using:

.. code-block:: bash

   vagrant ssh


Initial VM Development Setup
----------------------------

If you intend to develop the VM configuration (rather than simply *use* the
VM); immediately after cloning the repository run the following in the root
of the work area.

.. code-block:: bash

   poetry install
   poetry shell
   pre-commit install

The ``pre-commit`` command will ensure that the ``git`` repository is prepared.

On any subsequent session you need only ``poetry shell`` to ensure the
relevant tools are available.

Refer to the `full documentation <full-documentation_>`_ for details on development requirements
and process.
