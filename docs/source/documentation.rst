Documentation
=============

The documentation system.

Documentation for the VM is produced using `Sphinx <https://www.sphinx-doc.org/en/master/>`_.




General
-------

All documentation should be written in `reStructeredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_.



Writing Requirements
--------------------

Requirements can be captured anyway you like, but generally the use of `Gherkin <https://cucumber.io/docs/gherkin/>`_ is encouraged as this fits well with our preferred testing frameworks; `pytest <https://docs.pytest.org>`_, `pytest-bdd <https://pytest-bdd.readthedocs.io/en/latest/>`_ and `testinfra <https://testinfra.readthedocs.io/en/latest/>`_.

Feature files placed in ``source/features`` will be processed into ``rst`` files under ``sources/requirements/features`` and included in the main documentation just like any other ``rst`` documentation.

The ``scripts/docs.sh`` script runs Sphinx in a docker container (as defined in `svdocker/documentation <https://gitlab.com/svdocker/documentation>`_) using the ``--pre-build`` option to run `sphinx-gherkin <https://jolly-good-toolbelt.github.io/sphinx_gherkindoc/>`_ to transcribe ``sources/features``.
