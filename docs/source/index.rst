.. Developer (base) VM documentation master file, created by
   sphinx-quickstart on Thu Jan 12 10:36:23 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Developer (base) VM's documentation!
===============================================

This is the base developer VM setup used in SaltyVagrant projects.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage
   development
   documentation

   sls

   issues



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
