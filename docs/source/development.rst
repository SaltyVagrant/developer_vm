Development
===========

Developing the VM itself.



Development Environment
-----------------------

Once the repository is cloned set up a virtual environment for development.

.. code-block:: bash

   poetry install
   poetry shell

The main tools installed in this environment are ``pre-commit`` and ``commitizen``.

Once in the virtual environment we need to set up the ``git`` repository (assuming
we intend to push changes back).

.. code-block:: bash

   pre-commit install


Documentation
^^^^^^^^^^^^^

This documentation can be found in ``docs/sources``. The simplest way to
develop this documentation is using the ``scripts/docs.sh`` script to run
a live sphinx server. By default this will run a web server on ``localhost``
port ``8000`` where any documentation in ``docs/sources`` will be auto-built
and rendered.

The port to which the server is mapped can be managed using the environment
variable ``DOCS_PORT``, for example:

.. code-block:: bash

   DOCS_PORT=8003 ./scripts/docs.sh


For details of how the documentation for this VM is generated refer to :doc:`documentation`.



Base VM
-------

The base VM is in https://gitlab.com/SaltyVagrant/developer_vm.

The ``Vagrantfile`` should be as stable as possible with VM setup in three sub-directories:

.. _salt-config:

``salt.config.d``
^^^^^^^^^^^^^^^^^

This directory contains configuration files containing details for obtaining Salt configuration.

Each file is named for the scope it specifies (base, project, personal) with file extension ``.salt``  and must, at a minimum, set the environment variable ``SALT_CONFIG_REPO``.

The ``SALT_CONFIG_REPO`` variable is set to the URI of  a Git repository containing Salt configuration data.

The `base.salt <https://gitlab.com/SaltyVagrant/developer_vm/-/blob/master/salt.config.d/base.salt>`_ file can be used as an example and the `base Salt configuration repository <https://gitlab.com/salt-configurato/base_provisioning>`_ can be used as an example of how the Salt configuration repository should be structured.

.. _secure-provisioning:

``secure_provisioning``
^^^^^^^^^^^^^^^^^^^^^^^

This directory, if provided at all, should contain a ``.ssh`` directory containing any SSH configuration your ``vagrant`` account requires. This is typically the private SSH keys for remote Git repositories that you want to develop against.

``provision``
^^^^^^^^^^^^^

This contains the main provisioning script `core Script`_ and a handful of standard configuration files.


``core`` Script
"""""""""""""""

This is a simple provisioning script that:

* install SSH keys from :ref:`secure-provisioning`
* acquires and sets up Salt
* set up core Salt configuration
* all the configuration required for the VM (see :ref:`salt-config`)
* install salt formulae (using ``salt.core.formulae`` state
* runs Salt ``hoghstate`` to complete the provisioning.


Project VMs
-----------

Additional configuration specific to a project is placed in a Salt configuration repository and referenced in a ``project.salt`` file.
This configuration follows the same pattern as the `base Salt configuration repository <https://gitlab.com/salt-configurato/base_provisioning>`_.
