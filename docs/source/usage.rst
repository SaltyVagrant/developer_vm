Usage
=====

Once the project repository is cloned ``cd <workarea>`` and ``vagrant up``
should be enough to set up a new VM.

Once the new VM is created ``vagrant ssh`` to connect in to the new VM.

Personal Configuration
----------------------

You can supply a personal configuration using a ``.salt`` configuration as
described in :ref:`salt-config`.

.. note::

   As usual in Salt, the personal configuration is run in ``root`` context, so
   be sure to specify ``runas: vagrant`` if it is important.



Supplying Secrets
-----------------

It is sometimes necessary to supply secrets into the VM.

Currently the most important are the ``ssh`` keys to access ``git``
repositories. These can be supplied in ``secure_provisioning/ssh``.

.. note::

   ``secure_provisioning/ssh`` is handled specially by the :ref:`core-script`.

..
   To minimize conflicts, where possible, only add project
   specific information below this line.
