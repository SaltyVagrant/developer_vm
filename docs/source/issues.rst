Issues
======

The following outstanding issues remain:

#. Finish documentation
#. Resolve how to differentiate base documentation from derived project documentation.

   This should clone to a cache directory, then symlink the formula into a
   ``/srv/formula`` directory which we supply to the minion configuration.
