#!/usr/bin/env bash
# vim:ft=bash:

if [ "$0" = "${BASH_SOURCE[0]}" ]; then
  set -Eeuo pipefail
fi

DOCS_PORT=${DOCS_PORT:-8000}

CLEANUP_DONE=false
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
readonly DIR
PROJ_ROOT_DIR="$( cd "${DIR}/.." >/dev/null 2>&1 && pwd )"


cleanup () {
  [ "${CLEANUP_DONE}" = "true" ] && return

  CLEANUP_DONE=true
}

run_docs_autobuild () {
  docker run --rm -u "$(id -u):$(id -g)" -p "${DOCS_PORT}:8000" -v "${PROJ_ROOT_DIR}:/proj" -d saltyvagrant/documentation:latest sphinx-autobuild --host 0.0.0.0 --pre-build "sphinx-gherkindoc --step-glossary step-glossary /proj/docs/source/features /proj/docs/source/requirements/features" /proj/docs/source /proj/docs/source/_build/html
}


if [ "$0" = "${BASH_SOURCE[0]}" ]; then
  trap cleanup INT ERR EXIT

  run_docs_autobuild
fi
